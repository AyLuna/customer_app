import 'package:url_launcher/url_launcher.dart';

class MapsUtils{
  MapsUtils._();

  //latitude and longitude
  static Future<void> openMapWithPosition(double latitude, double longitude) async
  {
    String googleMapUrl="https://www.google.com/maps/search/?api=1&query=$latitude,$longitude";
    // ignore: deprecated_member_use
    if(await canLaunch(googleMapUrl)){await launch(googleMapUrl);}
    else{throw "Couldn't open the map";}
  }


  //text adress
  static Future<void> openMapWithAdress(String fullAdress) async
  {
    String query=Uri.encodeComponent(fullAdress);
    String googleMapUrl="https://www.google.com/maps/search/?api=1&query=$query";
    // ignore: deprecated_member_use
    if(await canLaunch(googleMapUrl)){await launch(googleMapUrl);}
    else{throw "Couldn't open the map";}
  }
}