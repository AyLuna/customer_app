import 'package:flutter/cupertino.dart';
import '../global/global.dart';

class CartItemCounter extends ChangeNotifier{
  int cartListItemCount=sharedPreferences!.getStringList("userCart")!.length-1;//garbage value ayirmak ucin 

  int get count=>cartListItemCount;

  Future<void>displayCartListItemsNumber() async{
    cartListItemCount=sharedPreferences!.getStringList("userCart")!.length-1;

    await Future.delayed(const Duration(milliseconds: 100), () {
      notifyListeners();    // setState(() {});
    });
  }
}