import 'package:alyjy/widgets/styled_text_widget.dart';
import 'package:flutter/material.dart';
import '../mainScreens/items_screen.dart';
import '../modal/sellers.dart';

class SellersUiDesignWidget extends StatefulWidget
{
  Sellers? model;
  BuildContext? context;

  SellersUiDesignWidget({this.model, this.context});

  @override
  _SellersUiDesignWidgetState createState() => _SellersUiDesignWidgetState();
}



class _SellersUiDesignWidgetState extends State<SellersUiDesignWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        //Navigator.push(context, MaterialPageRoute(builder: (c)=>ItemsScreen(model:widget.model)));
      },
      splashColor: Colors.amber,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 2,
          )],
          borderRadius: BorderRadius.circular(5),
        ),
        height: MediaQuery.of(context).size.height*0.30,
        width: MediaQuery.of(context).size.width*0.33-20,
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height*0.25,
              decoration: BoxDecoration(
                image: DecorationImage(image: NetworkImage(widget.model!.sellerAvatarUrl!),fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(5),
              ),
            ),
            Center(child:StyledTextWidget(text: widget.model!.sellerName!,color:Colors.cyan),),
                ],
              ),
            ),
    );
  }
}
