import 'package:flutter/material.dart';
import '../assistendMethods/cart_item_coumter.dart';
import '../mainScreens/bottom_nav_bar_screen.dart';
import '../mainScreens/cart_screen.dart';
import 'package:provider/provider.dart';
import '../mainScreens/items_screen.dart';
import 'styled_text_widget.dart';

class MyAppBar extends StatefulWidget{
  final String? sellerUID;


  const MyAppBar({Key? key, this.sellerUID}) : super(key: key);

  @override
  State<MyAppBar> createState() => _MyAppBarState();


}

class _MyAppBarState extends State<MyAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
        iconTheme: IconThemeData(
            color:Color.fromRGBO(72, 72, 72, 1), size: 25
        ),
        leading:IconButton(onPressed: (){
          Navigator.pop(context);
        }, icon: Icon(Icons.arrow_back_ios_new, color:Color.fromRGBO(72, 72, 72, 1))),
        flexibleSpace: Container(
          color: Colors.white,
        ),
        centerTitle: true,
        title: StyledTextWidget(text:"Bridge",size: 24,spacing: 3,weight:FontWeight.bold,color: Color.fromRGBO(240, 86, 52, 1),),
        actions: [
          Stack(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (c) =>
                                  CartScreen(sellerUID: widget.sellerUID)));
                    },
                    icon: Icon(Icons.shopping_cart,color: Color.fromRGBO(240, 86, 52, 1),)),
                Positioned(
                    top: 0.2,
                    left: 0,
                    child: Stack(
                      children: [
                        Container(
                          width: 15,
                          height: 15,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            border: Border.all(color: Color.fromRGBO(240, 86, 52, 1)),
                          ),
                          child: Center(
                            child: Consumer<CartItemCounter>(
                              builder: (context, counter, c) {
                                return StyledTextWidget(text: counter.count.toString(),color: Color.fromRGBO(240, 86, 52, 1),size:9);
                              },
                            ),
                          ),
                        ),
                      ],
                    )),
              ])]);
  }
}
