import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../generated/locale_keys.g.dart';
import '../modal/items.dart';
import 'styled_text_widget.dart';
class CartItemDesign extends StatefulWidget {
  final Items? model;
  final BuildContext? context;
  final int? quantityNumber;

  const CartItemDesign({Key? key, this.model, this.context, this.quantityNumber}) : super(key: key);



  @override
  State<CartItemDesign> createState() => _CartItemDesignState();
}

class _CartItemDesignState extends State<CartItemDesign> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
                splashColor: Colors.cyan,
                child: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Container(
                    height: 100,
                    width: MediaQuery.of(context).size.width-10,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Row(
                        children: [
                          Container(
                            width: 100, height: 100,
                            decoration: BoxDecoration(
                              image: DecorationImage(image: NetworkImage(widget.model!.thumbnailUrl!),fit: BoxFit.cover),
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: Color.fromRGBO(240, 86, 52, 1)),
                            ),
                          ),
                          SizedBox(width: 7,),

                          //title
                          //quantiy number
                          //price
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              //title
                              StyledTextWidget(text: widget.model!.title!),
                              SizedBox(height: 1,),

                              // X
                              //quantity number
                              Row(
                                children: [
                                  StyledTextWidget(text: "x ",color: Colors.cyan),
                                  StyledTextWidget(text: widget.quantityNumber.toString(),color: Colors.cyan),
                                ],
                              ),
                              SizedBox(height: 1,),

                              //price
                              Row(
                                children: [
                                  StyledTextWidget(text: LocaleKeys.Price.tr(),weight:FontWeight.bold),
                                  StyledTextWidget(text: widget.model!.price!.toString()+LocaleKeys.manat.tr(), size: 18, weight:FontWeight.bold),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
  }
}
