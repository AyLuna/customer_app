import 'package:alyjy/widgets/styled_text_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../assistendMethods/adress_changer.dart';
import '../generated/locale_keys.g.dart';
import '../maps/maps.dart';
import 'package:provider/provider.dart';
import '../mainScreens/place_order_screen.dart';
import '../modal/adress.dart';

class AdressUIDesign extends StatefulWidget {
  final Adress? model;
  final int? currentIndex;
  final int? value;
  final String? adressID;
  final double? totalAmount;
  final String? sellerUID;

  const AdressUIDesign({Key? key, this.model, this.currentIndex, this.value, this.adressID, this.totalAmount, this.sellerUID}) : super(key: key);

  @override
  State<AdressUIDesign> createState() => _AdressUIDesignState();
}

class _AdressUIDesignState extends State<AdressUIDesign> {

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: (){
          //select this adress
          Provider.of<AdressChanger>(context, listen: false).displayResult(widget.value);
        },
        child: Card(
          child: Column(
            children: [
              //adress info
             Row(
               children: [
                 Radio(value: widget.value!, groupValue: widget.currentIndex!, activeColor: Colors.amber, onChanged: (val){
                   //provider
                   Provider.of<AdressChanger>(context, listen: false).displayResult(val);
                   }),
                 Column(
                   crossAxisAlignment:CrossAxisAlignment.start,
                   children: [
                     Container(
                       padding: EdgeInsets.all(10),
                       width: MediaQuery.of(context).size.width*0.8,
                       child:
                       Table(
                         children: [
                           TableRow(
                             children: [
                               StyledTextWidget(text: LocaleKeys.Name.tr(), weight:FontWeight.bold),
                               StyledTextWidget(text: widget.model!.name.toString(), weight:FontWeight.bold),
                             ]
                           ),
                           TableRow(
                               children: [
                                 StyledTextWidget(text: LocaleKeys.Phone.tr(), weight:FontWeight.bold),
                                 StyledTextWidget(text: widget.model!.phoneNumber.toString(), weight:FontWeight.bold),
                               ]
                           ),
                           TableRow(
                               children: [
                                 StyledTextWidget(text: LocaleKeys.Flat_Number.tr(), weight:FontWeight.bold),
                                 StyledTextWidget(text: widget.model!.flatNumber.toString(), weight:FontWeight.bold),
                               ]
                           ),
                           TableRow(
                               children: [
                                 StyledTextWidget(text: LocaleKeys.City.tr(), weight:FontWeight.bold),
                                 StyledTextWidget(text: widget.model!.city.toString(), weight:FontWeight.bold),

                               ]
                           ),
                           TableRow(
                               children: [
                                 StyledTextWidget(text: LocaleKeys.State.tr(), weight:FontWeight.bold),
                                 StyledTextWidget(text: widget.model!.state.toString(), weight:FontWeight.bold),
                               ]
                           ),
                           TableRow(
                               children: [
                                 StyledTextWidget(text: LocaleKeys.Complete_Adress.tr(), weight:FontWeight.bold),
                                 StyledTextWidget(text: widget.model!.fullAdress.toString(), weight:FontWeight.bold),
                               ]
                           ),
                         ],
                       ),
                     ),
                   ],
                 ),
               ],
             ),

              //button1
              ElevatedButton(onPressed: (){
                MapsUtils.openMapWithPosition(widget.model!.lat!, widget.model!.lng!);

                //MapsUtils.openMapWithAdress(widget.model!.fullAdress!);
              }, style: ElevatedButton.styleFrom(primary: Colors.black54),child: Text(LocaleKeys.Check_in_Maps.tr())),

              //button2
              widget.value==Provider.of<AdressChanger>(context).count?ElevatedButton(onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (c)=> PlaceOrderScreen(adressID: widget.adressID, totalAmount: widget.totalAmount, sellerUID: widget.sellerUID,)));
              }, style: ElevatedButton.styleFrom(
                  primary: Colors.cyan), child: Text(LocaleKeys.Proceed.tr())):Container(),

              
            ],
          ),
        ));
  }
}
