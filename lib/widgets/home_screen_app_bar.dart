import 'package:alyjy/widgets/styled_text_widget.dart';
import 'package:flutter/material.dart';

import '../mainScreens/search_screen.dart';
import 'language_picker.dart';
class HomeScreenAppBar extends StatefulWidget{
  final String? sellerUID;

  const HomeScreenAppBar({Key? key, this.sellerUID}) : super(key: key);

  @override
  State<HomeScreenAppBar> createState() => _HomeScreenAppBarState();

}

class _HomeScreenAppBarState extends State<HomeScreenAppBar> {
  @override
  Widget build(BuildContext context) {
    return
      AppBar(
        automaticallyImplyLeading: false,
        iconTheme: IconThemeData(
            color:Color.fromRGBO(72, 72, 72, 1), size: 25
        ),
        flexibleSpace: Container(
          color: Colors.white,
        ),
        title: Row(
          children: [
            LanguagePicker(),
            Padding(
              padding: const EdgeInsets.only(left: 80),
              child: StyledTextWidget(text: "Bridge",size: 24,spacing: 3,weight:FontWeight.bold,color: Color.fromRGBO(240, 86, 52, 1),),
            ),
          ],
        ),
        actions: [
          IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (c)=>SearchScreen()));
          }, icon: Icon(Icons.search)),
        ],
      );
  }
}

