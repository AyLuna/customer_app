import 'package:alyjy/widgets/styled_text_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../generated/locale_keys.g.dart';
import '../mainScreens/bottom_nav_bar_screen.dart';
import '../mainScreens/home_screen.dart';
import '../modal/adress.dart';

class ShipmentAddressDesign extends StatelessWidget
{
  final Adress? model;

  ShipmentAddressDesign({this.model});



  @override
  Widget build(BuildContext context)
  {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.all(10.0),
          child: StyledTextWidget(text: LocaleKeys.Shipping_Details.tr(), weight: FontWeight.bold, size: 18,),
        ),
        const SizedBox(
          height: 6.0,
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 5),
          width: MediaQuery.of(context).size.width-10,
          child: Table(
            children: [
              TableRow(
                  children: [
                    StyledTextWidget(text: LocaleKeys.Name.tr()),
                    StyledTextWidget(text: model!.name!),
                  ]
              ),
              TableRow(
                  children: [
                    StyledTextWidget(text: LocaleKeys.Phone.tr()),
                    StyledTextWidget(text: model!.phoneNumber!),
                  ]
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child:StyledTextWidget(text: model!.fullAdress!),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: InkWell(
              onTap: ()
              {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const BottomNavBarScreen()));
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(240, 86, 52, 1),
                  borderRadius: BorderRadius.circular(20),),
                width: MediaQuery.of(context).size.width - 70,
                height: 70,
                child: Center(
                  child: StyledTextWidget(text: LocaleKeys.Go_Back.tr(), color:Colors.white)
                  ),
                ),
              ),
            ),
          ),
      ],
    );
  }
}
