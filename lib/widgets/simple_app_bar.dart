import 'package:flutter/material.dart';
import '../mainScreens/bottom_nav_bar_screen.dart';
import 'styled_text_widget.dart';

class SimpleAppBar extends StatelessWidget
{
  String? title;


  SimpleAppBar({this.title,});



  @override
  Widget build(BuildContext context)
  {
    return AppBar(
      iconTheme: IconThemeData(
          color:Color.fromRGBO(72, 72, 72, 1), size: 25
      ),
      leading:IconButton(onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context) => const BottomNavBarScreen())), icon: Icon(Icons.arrow_back_ios_new,)),
      flexibleSpace: Container(
        color: Colors.white,
      ),
      centerTitle: true,
      title: StyledTextWidget(text: title!,size: 24,spacing: 3,weight:FontWeight.bold,color: Color.fromRGBO(240, 86, 52, 1),),
    );
  }
}

