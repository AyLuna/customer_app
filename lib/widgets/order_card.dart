import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../generated/locale_keys.g.dart';
import '../mainScreens/order_detail_screen.dart';
import '../modal/items.dart';
import 'styled_text_widget.dart';


class OrderCard extends StatelessWidget
{
  final int? itemCount;
  final List<DocumentSnapshot>? data;
  final String? orderID;
  final List<String>? seperateQuantitiesList;

  OrderCard({
    this.itemCount,
    this.data,
    this.orderID,
    this.seperateQuantitiesList,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()
      {
        Navigator.push(context, MaterialPageRoute(builder: (c)=> OrderDetailsScreen(orderID: orderID)));
      },
      child: Card(
        elevation: 1,
        child: Container(
          height: itemCount! * 112,
          child: ListView.builder(
            itemCount: itemCount,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index)
            {
              Items model = Items.fromJson(data![index].data()! as Map<String, dynamic>);
              return placedOrderDesignWidget(model, context, seperateQuantitiesList![index]);
            },
          ),
        ),
      ),
    );
  }
}




Widget placedOrderDesignWidget(Items model, BuildContext context, seperateQuantitiesList)
{
  return Row(
    crossAxisAlignment:CrossAxisAlignment.start,
    mainAxisAlignment:MainAxisAlignment.start,
    children: [
      Padding(
        padding: const EdgeInsets.all(5),
        child: Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
            image: DecorationImage(image: NetworkImage(model.thumbnailUrl!),fit: BoxFit.cover),
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color:Color.fromRGBO(240, 86, 52, 1)),
          ),
        ),
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 5, 0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                StyledTextWidget(text: model.title!),
                Padding(
                  padding: const EdgeInsets.only(left:3,right: 5),
                  child: StyledTextWidget(text:  model.price.toString()+LocaleKeys.manat.tr()),
                ),
              ],
            ),
          ),
          Row(
            children: [
              StyledTextWidget(text: "X ",color:Colors.cyan, weight:FontWeight.bold),
              StyledTextWidget(text: seperateQuantitiesList, color:Colors.cyan, weight:FontWeight.bold),
            ],
          ),
        ],
      ),
    ],
  );
}
