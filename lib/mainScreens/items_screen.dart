import 'package:alyjy/mainScreens/bottom_nav_bar_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';
import '../assistendMethods/cart_item_coumter.dart';
import '../modal/items.dart';
import '../modal/sellers.dart';
import '../widgets/app_bar.dart';
import '../widgets/items_ui_design.dart';
import '../widgets/header_text_widget.dart';
import '../widgets/progress_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../global/global.dart';
import '../widgets/styled_text_widget.dart';
import 'cart_screen.dart';

class ItemsScreen extends StatefulWidget {

  Sellers? model;
  Items? Mmodel;

  ItemsScreen({Key? key, this.model, this.Mmodel}) : super(key: key);





  @override
  State<ItemsScreen> createState() => _ItemsScreenState();
}

class _ItemsScreenState extends State<ItemsScreen> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async =>false,
      child: Scaffold(
        appBar:MyAppBar(sellerUID:widget.model!.sellerUID),
        body:CustomScrollView(
          slivers: [
            StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance.collection("sellers").doc(widget.model!.sellerUID).collection("items").orderBy("publishedDate", descending: true).snapshots(),
                builder: (context, snapshot){
                  return !snapshot.hasData?SliverToBoxAdapter(child: Center(child: circularProgress(),),)
                  :SliverStaggeredGrid.countBuilder(crossAxisCount: 2, staggeredTileBuilder: (c)=>StaggeredTile.fit(1), itemBuilder: (context, index){
                    Items model=Items.fromJson(
                      snapshot.data!.docs[index].data()!as Map<String, dynamic>,
                    );
                    return Padding(
                      padding: const EdgeInsets.only(left:10, right: 10, top: 10),
                      child: ItemsUiDesignWidget(
                        model: model,
                        context: context,
                      ),
                    );
                  }, itemCount: snapshot.data!.docs.length);
                }),
        ]),
      ),
    );
  }
}
