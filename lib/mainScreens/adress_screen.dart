import 'package:alyjy/widgets/styled_text_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../assistendMethods/adress_changer.dart';
import '../generated/locale_keys.g.dart';
import '../mainScreens/save_adress_screen.dart';
import '../modal/adress.dart';
import '../widgets/adress_ui_design.dart';
import '../widgets/simple_app_bar.dart';
import 'package:provider/provider.dart';
import '../global/global.dart';
import '../widgets/progress_bar.dart';
import 'bottom_nav_bar_screen.dart';

class AdressScreen extends StatefulWidget
{
  final double? totalAmount;
  final String? sellerUID;

  AdressScreen({this.totalAmount, this.sellerUID});


  @override
  _AdressScreenState createState() => _AdressScreenState();
}



class _AdressScreenState extends State<AdressScreen>
{
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async =>false,
        child: Scaffold(
          appBar:SimpleAppBar(title: "Bridge",),
          floatingActionButton: FloatingActionButton.extended(
            label: Text(LocaleKeys.Add_New_Adress.tr()),
            backgroundColor: Colors.cyan,
            icon: const Icon(Icons.add_location, color: Colors.white,),
            onPressed: ()
            {
              //save address to user collection
              Navigator.push(context, MaterialPageRoute(builder: (c)=> SaveAdreesScreen()));
            },
            heroTag: "btn3",
          ),
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child:StyledTextWidget(text: LocaleKeys.Select_Adress.tr(),size: 20,weight: FontWeight.bold,),
                  ),
                ),

                Consumer<AdressChanger>(builder: (context, address, c){
                  return Flexible(
                    child: StreamBuilder<QuerySnapshot>(
                      stream: FirebaseFirestore.instance.collection("users")
                          .doc(sharedPreferences!.getString("uid"))
                          .collection("userAdress")
                          .snapshots(),
                      builder: (context, snapshot)
                      {
                        return !snapshot.hasData
                            ? Center(child: circularProgress(),)
                            : snapshot.data!.docs.length == 0
                            ? Container()
                            : ListView.builder(
                          itemCount: snapshot.data!.docs.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index)
                          {
                            return AdressUIDesign(
                              currentIndex: address.count,
                              value: index,
                              adressID: snapshot.data!.docs[index].id,
                              totalAmount: widget.totalAmount,
                              sellerUID: widget.sellerUID,
                              model: Adress.fromJson(
                                  snapshot.data!.docs[index].data()! as Map<String, dynamic>
                              ),
                            );
                          },
                        );

                      },
                    ),
                  );
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
