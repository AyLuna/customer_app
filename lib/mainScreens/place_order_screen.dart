import 'package:alyjy/mainScreens/my_orders_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../assistendMethods/assistant_methods.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';
import '../mainScreens/home_screen.dart';
import '../widgets/simple_app_bar.dart';
import 'bottom_nav_bar_screen.dart';
class PlaceOrderScreen extends StatefulWidget {
  double? totalAmount;
  String? adressID;
  String? sellerUID;

  PlaceOrderScreen({Key? key, this.totalAmount, this.adressID, this.sellerUID}) : super(key: key);


  @override
  State<PlaceOrderScreen> createState() => _PlaceOrderScreenState();
}



class _PlaceOrderScreenState extends State<PlaceOrderScreen> {
  String orderId=DateTime.now().microsecondsSinceEpoch.toString();

  addOrderDetails(){
    writeOrderDetailsForUser({
      "adressId":widget.adressID,
      "totalAmount":widget.totalAmount,
      "orderBy":sharedPreferences!.getString("uid"),
      "productIDs":sharedPreferences!.getStringList("userCart"),
      "paymentDetails":"Cash on Delivery",
      "orderTime":orderId,
      "isSuccess":true,
      "sellerUID":widget.sellerUID,
      "riderUID":"",
      "status":"normal",
      "orderId":orderId,
    });

    writeOrderDetailsForSeller({
      "adressId":widget.adressID,
      "totalAmount":widget.totalAmount,
      "orderBy":sharedPreferences!.getString("uid"),
      "productIDs":sharedPreferences!.getStringList("userCart"),
      "paymentDetails":"Cash on Delivery",
      "orderTime":orderId,
      "isSuccess":true,
      "sellerUID":widget.sellerUID,
      "riderUID":"",
      "status":"normal",
      "orderId":orderId,
    }).whenComplete((){
      clearCartNow(context);
      setState(() {
        orderId="";
        Navigator.push(context, MaterialPageRoute(builder: (c)=>BottomNavBarScreen()));
        Fluttertoast.showToast(msg: LocaleKeys.Order_has_been_placed_successfully.tr());
      });
    });
  }

  Future writeOrderDetailsForUser(Map<String, dynamic> data) async{
    await FirebaseFirestore.instance.collection("users").doc(sharedPreferences!.getString("uid")).collection("orders").doc(orderId).set(data);
  }
  Future writeOrderDetailsForSeller(Map<String, dynamic> data) async{
    await FirebaseFirestore.instance.collection("orders").doc(orderId).set(data);
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async =>false,
        child: Scaffold(
            appBar: SimpleAppBar(title: LocaleKeys.Order_Detail.tr(),),
            body: Material(child: Container(
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("images/food-delivery.png", height: 220,),

                  SizedBox(height: 12,),

                  ElevatedButton(onPressed: (){
                   addOrderDetails();
                  }, style: ElevatedButton.styleFrom(
                      primary: Colors.cyan), child: Text(LocaleKeys.Place_order.tr())),
                ],
              ),
            ))),
      ),
    );
  }
}
