import 'package:alyjy/widgets/styled_text_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:number_inc_dec/number_inc_dec.dart';
import 'package:provider/provider.dart';
import '../assistendMethods/cart_item_coumter.dart';
import '../generated/locale_keys.g.dart';
import '../widgets/app_bar.dart';
import '../assistendMethods/assistant_methods.dart';
import '../modal/items.dart';
import 'cart_screen.dart';
import 'items_screen.dart';

class ItemsDetailScreen extends StatefulWidget {
  final Items? model;

  const ItemsDetailScreen({Key? key, this.model}) : super(key: key);


  @override
  State<ItemsDetailScreen> createState() => _ItemsDetailScreenState();
}

class _ItemsDetailScreenState extends State<ItemsDetailScreen> {
  TextEditingController counterController=TextEditingController();




  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async =>false,
        child: Scaffold(
          appBar:MyAppBar(sellerUID:widget.model!.sellerUID),
          body:SingleChildScrollView(
            child: Column(
              crossAxisAlignment:CrossAxisAlignment.start,
              children: [
                Container(
                  height: MediaQuery.of(context).size.height*0.45,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: NetworkImage(widget.model!.thumbnailUrl!),fit: BoxFit.cover),
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: NumberInputPrefabbed.roundedEdgeButtons(controller: counterController, decIconSize: 55,incIconSize: 55,
                  incDecBgColor: Colors.amber,
                  min: 1,
                  max: 100,
                  initialValue: 1,
                  buttonArrangement:ButtonArrangement.incRightDecLeft,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: StyledTextWidget(text: widget.model!.title.toString(), weight: FontWeight.bold,size: 20,),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child:StyledTextWidget(text: widget.model!.longDescription.toString(), maxLines: 4,overflow: TextOverflow.fade,),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child:  StyledTextWidget(text: widget.model!.price.toString()+LocaleKeys.manat.tr(), weight: FontWeight.bold,),),
                SizedBox(height: 10,),
                Center(
                  child: InkWell(
                    onTap: (){
                      int itemCounter=int.parse(counterController.text);

                      List<String>seperateItemIDsList= seperateItemIDs();

                      //1.check if item exists in items list
                      seperateItemIDsList.contains(widget.model!.itemID)?Fluttertoast.showToast(msg: LocaleKeys.Item_is_already_added_to_Cart.tr()):addItemToCard(widget.model!.itemID, context,itemCounter);

                      //2.add to a cart
                      addItemToCard(widget.model!.itemID, context,itemCounter);

                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(240, 86, 52, 1),
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: Colors.grey),),
                      width: MediaQuery.of(context).size.width-70,
                      height: 70,
                      child: Center(child: StyledTextWidget(text: LocaleKeys.Add_to_card.tr(),color:Colors.white)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }



}



