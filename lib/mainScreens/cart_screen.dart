import 'package:alyjy/mainScreens/bottom_nav_bar_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../assistendMethods/assistant_methods.dart';
import '../assistendMethods/total_amount.dart';
import '../generated/locale_keys.g.dart';
import '../mainScreens/adress_screen.dart';
import '../modal/items.dart';
import '../widgets/cart_item_design.dart';
import 'package:provider/provider.dart';
import '../assistendMethods/cart_item_coumter.dart';
import '../widgets/progress_bar.dart';
import '../widgets/styled_text_widget.dart';
class CartScreen extends StatefulWidget {
  final String? sellerUID;

  const CartScreen({Key? key, this.sellerUID}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  List<int>? seperateItemQuantitiesList;
  num totalAmount=0;

  @override
  void initState() {

    super.initState();
    totalAmount=0;
    Provider.of<TotalAmount>(context, listen: false).displayTotalAmount(0);

    seperateItemQuantitiesList=seperateItemQuantities();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar:AppBar(
          iconTheme: IconThemeData(
              color:Color.fromRGBO(72, 72, 72, 1), size: 25
          ),
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
            color: Colors.white,
          ),
          centerTitle: true,
          title: StyledTextWidget(text: LocaleKeys.My_Cart_List.tr(),size: 24,spacing: 3,weight:FontWeight.bold,color: Color.fromRGBO(240, 86, 52, 1),),
        ),
        floatingActionButton: Row(
          mainAxisAlignment:MainAxisAlignment.spaceAround,
          children: [
            SizedBox(width: 8,),
            Align(
              alignment: Alignment.bottomLeft,
              child: FloatingActionButton.extended(
                heroTag: "btn1",
                label: Text(LocaleKeys.Clear_Cart.tr()),
                backgroundColor: Colors.cyan,
                icon: Icon(Icons.clear_all),
                onPressed: (){
                clearCartNow(context);

                Navigator.push(context, MaterialPageRoute(builder: (c)=>BottomNavBarScreen()));

                Fluttertoast.showToast(msg: LocaleKeys.Cart_has_been_cleared.tr());
              },
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: FloatingActionButton.extended(
                heroTag: "btn2",
                label: Text(LocaleKeys.Check_Out.tr()),
                backgroundColor: Colors.cyan,
                icon: Icon(Icons.navigate_next),
                onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (c)=>AdressScreen(totalAmount:totalAmount.toDouble(), sellerUID:widget.sellerUID,)));
              },
                  ),
            ),
          ],
        ),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Consumer2<TotalAmount, CartItemCounter>(builder:(context, amountProvider, cartProvider,c){
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(child: cartProvider.count==0?Container():StyledTextWidget(text:LocaleKeys.Total_Price.tr()+ amountProvider.tAmount.toString(), size:20, weight:FontWeight.w500),),

                );
              }),
            ),

            //display cart items with quantity number
            StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance.collection("items").where("itemID", whereIn: seperateItemIDs()).orderBy("publishedDate", descending:true).snapshots(),
                builder: (context, snapshot){
                  return!snapshot.hasData?SliverToBoxAdapter(child: Center(child: circularProgress(),),)
                      :snapshot.data!.docs.length==0?SliverToBoxAdapter(child: Container())
                      :SliverList(delegate: SliverChildBuilderDelegate((context, index) {
                        Items model=Items.fromJson(snapshot.data!.docs[index].data()! as Map<String, dynamic>,);

                        if(index==0){
                          totalAmount=0;
                          totalAmount=totalAmount+(model.price!*seperateItemQuantitiesList![index]);
                        }
                        else{
                          totalAmount=totalAmount+(model.price!*seperateItemQuantitiesList![index]);
                        }

                        if(snapshot.data!.docs.length-1==index){
                          WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
                            Provider.of<TotalAmount>(context, listen: false).displayTotalAmount(totalAmount.toDouble());
                          });
                        }

                        return CartItemDesign(
                          model:model,
                          context: context,
                          quantityNumber: seperateItemQuantitiesList![index],
                        );

                      },childCount: snapshot.hasData?snapshot.data!.docs.length:0));

                }),
          ],
        ),
      ),
    );
  }

}
