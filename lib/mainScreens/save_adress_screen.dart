import 'package:alyjy/widgets/simple_app_bar.dart';
import 'package:alyjy/widgets/styled_text_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import '../generated/locale_keys.g.dart';
import '../modal/adress.dart';
import '../global/global.dart';
import '../widgets/custom_text_field.dart';
import 'bottom_nav_bar_screen.dart';

class SaveAdreesScreen extends StatelessWidget {
  final _name = TextEditingController();
  final _phoneNumber = TextEditingController();
  final _flatNumber = TextEditingController();
  final _city = TextEditingController();
  final _state = TextEditingController();
  final _completeAdress = TextEditingController();
  final _locationController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  Position? position;
  List<Placemark>? placemarks;


  getCurrentLocation() async{
    Position newPosition = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );

    position=newPosition;

    placemarks= await placemarkFromCoordinates(position!.latitude, position!.longitude);

    Placemark pMark=placemarks![0];

    String fullAdress='${pMark.subThoroughfare} ${pMark.thoroughfare}, ${pMark.subLocality} ${pMark.locality}, ${pMark.subAdministrativeArea}, ${pMark.administrativeArea}, ${pMark.postalCode}, ${pMark.country}';

    _locationController.text=fullAdress;

    _flatNumber.text='${pMark.subThoroughfare} ${pMark.thoroughfare}, ${pMark.subLocality} ${pMark.locality}';
    _city.text='${pMark.subAdministrativeArea}, ${pMark.administrativeArea} ${pMark.postalCode}';
    _state.text='${pMark.country}';
    _completeAdress.text = fullAdress;
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async =>false,
      child: Scaffold(
          floatingActionButton: FloatingActionButton.extended(
              backgroundColor: Colors.cyan,
              icon: Icon(Icons.save),
              label: Text(LocaleKeys.Save_Now.tr()),
            heroTag: "btn4",
            onPressed: (){
              //save adress info
              if(formKey.currentState!.validate()){
                final model=Adress(
                  name:_name.text.trim(),
                  state:_state.text.trim(),
                  fullAdress: _completeAdress.text.trim(),
                  phoneNumber:_phoneNumber.text.trim(),
                  flatNumber:_flatNumber.text.trim(),
                  city:_city.text.trim(),
                  lat:position!.latitude,
                  lng:position!.longitude,
                ).toJson();

                FirebaseFirestore.instance.collection("users").doc(sharedPreferences!.getString("uid")).collection("userAdress").doc(DateTime.now().microsecondsSinceEpoch.toString()).set(model).then((value){
                  Fluttertoast.showToast(msg: LocaleKeys.New_Adress_has_been_added.tr());
                  formKey.currentState!.reset();
                });
              }
            },
              ),
          appBar: SimpleAppBar(title: "Bridge",),
          body: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 6,),
                Align(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:StyledTextWidget(text: LocaleKeys.Save_New_adress.tr(), size: 20, weight: FontWeight.bold,),
                  ),
                ),

                ListTile(
                  leading: Icon(
                    Icons.person_pin_circle,
                    color: Colors.black,
                    size: 35,
                  ),
                  title: Container(
                    width: 250,
                    child: TextField(
                      style: TextStyle(color: Colors.black),
                      controller: _locationController,
                      decoration: InputDecoration(
                        hintText: LocaleKeys.Whats_your_adress.tr(),
                        hintStyle: TextStyle(color: Colors.black),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 6,),
                ElevatedButton.icon(
                  label: Text(
                    LocaleKeys.Get_my_Current_Location.tr(),
                    style: TextStyle(color: Colors.white),
                  ),
                  icon: Icon(Icons.location_on, color: Colors.white),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                          side: BorderSide(color: Colors.cyan),
                        ),
                    ),
                  ),
                  onPressed: () {
                    getCurrentLocation();
                  },
                ),


                Form(
                  key: formKey,
                  child: Column(
                    children: [
                      CustomTextField(hintText: LocaleKeys.Name.tr(), controller: _name,isObsecre: false ,),
                      SizedBox(height: 6,),
                      CustomTextField(hintText: LocaleKeys.Phone.tr(), controller: _phoneNumber,isObsecre: false ,),
                      SizedBox(height: 6,),
                      CustomTextField(hintText: LocaleKeys.City.tr(), controller: _city,isObsecre: false ,),
                      SizedBox(height: 6,),
                      CustomTextField(hintText: LocaleKeys.State.tr(), controller: _state,isObsecre: false ,),
                      SizedBox(height: 6,),
                      CustomTextField(hintText: LocaleKeys.Adress_Line.tr(), controller: _flatNumber,isObsecre: false ,),
                      SizedBox(height: 6,),
                      CustomTextField(hintText: LocaleKeys.Complete_Adress.tr(), controller: _completeAdress,isObsecre: false ,),
                      SizedBox(height: 6,),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }


}
