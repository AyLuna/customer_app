import 'package:alyjy/widgets/styled_text_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';
import '../modal/adress.dart';
import '../widgets/progress_bar.dart';
import '../widgets/shipment_address_design.dart';
import '../widgets/status_banner.dart';
import 'package:intl/intl.dart';


class OrderDetailsScreen extends StatefulWidget
{
  final String? orderID;

  OrderDetailsScreen({this.orderID});

  @override
  _OrderDetailsScreenState createState() => _OrderDetailsScreenState();
}




class _OrderDetailsScreenState extends State<OrderDetailsScreen>
{
  String orderStatus = "";

  @override
  Widget build(BuildContext context)
  {
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async =>false,
        child: Scaffold(
          body: SingleChildScrollView(
            child: FutureBuilder<DocumentSnapshot>(
              future: FirebaseFirestore.instance
                  .collection("users")
                  .doc(sharedPreferences!.getString("uid"))
                  .collection("orders")
                  .doc(widget.orderID)
                  .get(),
              builder: (c, snapshot)
              {
                Map? dataMap;
                if(snapshot.hasData)
                {
                  dataMap = snapshot.data!.data()! as Map<String, dynamic>;
                  orderStatus = dataMap["status"].toString();
                }
                return snapshot.hasData
                    ? Container(
                  child: Column(
                    children: [
                      StatusBanner(
                        status: dataMap!["isSuccess"],
                        orderStatus: orderStatus,
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: StyledTextWidget(text: dataMap["totalAmount"].toString()+LocaleKeys.manat.tr(), size: 24, weight: FontWeight.bold,),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: StyledTextWidget(text: LocaleKeys.Order_Id.tr() + widget.orderID!),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: StyledTextWidget(text: LocaleKeys.Order_at.tr() + DateFormat("dd MMMM, yyyy - hh:mm aa").format(DateTime.fromMillisecondsSinceEpoch(int.parse(dataMap["orderTime"]))), color: Colors.grey,),
                      ),
                      const Divider(thickness: 4,),
                      orderStatus == "ended"
                          ? Image.asset("images/success.jpg")
                          : Image.asset("images/state.jpg"),
                      const Divider(thickness: 4,),
                      FutureBuilder<DocumentSnapshot>(
                        future: FirebaseFirestore.instance
                            .collection("users")
                            .doc(sharedPreferences!.getString("uid"))
                            .collection("userAdress")
                            .doc(dataMap["adressId"])
                            .get(),
                        builder: (c, snapshot)
                        {
                          return snapshot.hasData
                              ? ShipmentAddressDesign(
                            model: Adress.fromJson(
                                snapshot.data!.data()! as Map<String, dynamic>
                            ),
                          )
                              : Center(child: circularProgress(),);
                        },
                      ),
                    ],
                  ),
                )
                    : Center(child: circularProgress(),);
              },
            ),
          ),
        ),
      ),
    );
  }
}
