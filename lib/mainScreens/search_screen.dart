import 'package:alyjy/widgets/category_ui_design.dart';
import 'package:alyjy/widgets/custom_text_field.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../generated/locale_keys.g.dart';
import '../modal/items.dart';
import '../widgets/items_ui_design.dart';
import 'bottom_nav_bar_screen.dart';


class SearchScreen extends StatefulWidget
{
  final Items? model;

  const SearchScreen({Key? key, this.model}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}



class _SearchScreenState extends State<SearchScreen>
{
  Future<QuerySnapshot>? searchDocumentsList;
  String categoryNameText = "";


  initSearchingCategory(String textEntered)
  {
    searchDocumentsList = FirebaseFirestore.instance.collection("items")
        .where("categoryEng", isGreaterThanOrEqualTo: textEntered)
        .get();
  }

  
  @override
  Widget build(BuildContext context)
  {
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async =>false,
        child: Scaffold(
          appBar: AppBar(
            leading:IconButton(onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context) => const BottomNavBarScreen())), icon: Icon(Icons.arrow_back_ios_new,)),
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(
                color:Color.fromRGBO(72, 72, 72, 1), size: 35
            ),
            flexibleSpace: Container(
              decoration: BoxDecoration(
              ),
            ),
            title: TextField(
              onChanged: (textEntered)
                  {
                    setState(() {
                      categoryNameText= textEntered;
                    });
                    //init search
                    initSearchingCategory(textEntered);
              },
                 decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: LocaleKeys.Search_here.tr(),
                    hintStyle: const TextStyle(color: Colors.grey),
                    suffixIcon: IconButton(
                      icon: const Icon(Icons.search),
                      color: Colors.grey,
                      onPressed: ()
                      {
                        initSearchingCategory(categoryNameText);
                      },
                    ),
                  ),
              style: const TextStyle(
                color: Colors.grey,
                fontSize: 16,
              ),
            ),
          ),
          body: FutureBuilder<QuerySnapshot>(
                  future: searchDocumentsList,
                  builder: (context, snapshot)
                  {
                    return snapshot.hasData
                        ? ListView.builder(
                      itemCount: snapshot.data!.docs.length,
                      itemBuilder: (context, index)
                      {
                        Items model = Items.fromJson(
                            snapshot.data!.docs[index].data()! as Map<String, dynamic>
                        );

                           return ItemsUiDesignWidget(
                          model: model,
                          context: context,
                        );
                      },
                    )
                  : Center(child: Text(LocaleKeys.No_Record_Found.tr()),);
            },
          ),
        ),
      ),
    );
  }
}
