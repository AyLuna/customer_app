import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../categoryScreens/bags_screen.dart';
import '../categoryScreens/beads_screen.dart';
import '../categoryScreens/ceramics_screen.dart';
import '../categoryScreens/dolls_screen.dart';
import '../categoryScreens/embroids_screen.dart';
import '../categoryScreens/knitwear_screen.dart';
import '../categoryScreens/others_screen.dart';
import '../categoryScreens/paintings_screen.dart';
import '../categoryScreens/pastry_screens.dart';
import '../categoryScreens/rugs_screen.dart';
import '../generated/locale_keys.g.dart';
import '../widgets/app_bar.dart';
import '../widgets/simple_app_bar.dart';
import '../widgets/styled_text_widget.dart';
class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> with AutomaticKeepAliveClientMixin{
  final routes = [BagsScreen(),CeramicsScreen(),PaintingsScreen(),BeadsScreen(),EmbroidsScreen(),DollsScreen(),RugsScreen(),KnitwearScreen(),PastryScreen(),OthersScreen()];
  final categoryImages=["categoryImages/bag 1.png","categoryImages/pot 1.png","categoryImages/art 1.png","categoryImages/bracelet 1.png","categoryImages/embroidery (2) 1.png","categoryImages/doll 1.png","categoryImages/adornment 1.png","categoryImages/yarn-ball 1.png","categoryImages/cake-slice 1.png","categoryImages/plate 1.png",];
  final categoryList=["Bags","Ceramics","Paintings","Beads","Embroidery","Dolls","Rugs","Knitwear","Pastry","Others"];
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(child: WillPopScope(
      onWillPop: ()async =>false,
      child: Scaffold(
        appBar:AppBar(
          iconTheme: IconThemeData(
              color:Color.fromRGBO(72, 72, 72, 1), size: 25
          ),
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
            color: Colors.white,
          ),
          centerTitle: true,
          title: StyledTextWidget(text: LocaleKeys.Categories.tr(),size: 24,spacing: 3,weight:FontWeight.bold,color: Color.fromRGBO(240, 86, 52, 1),),
        ),
        body: CustomScrollView(
          slivers: [
            SliverPadding(padding: EdgeInsets.only(top:10),),
      SliverStaggeredGrid.countBuilder(crossAxisCount: 3, staggeredTileBuilder: (c)=>StaggeredTile.fit(1), itemBuilder: (context, index){
      return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
      width: 100,
      height: 130,
      decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(15),
      border: Border.all(color: Color.fromRGBO(240, 86, 52, 1)),),
      child: InkWell(
      onTap: ()
      {
      Navigator.push(context, MaterialPageRoute(builder: (c)=> routes[index]));
      },
      child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
      Center(
      child:Container(width:50,height:50,child: Image.asset(categoryImages[index], fit: BoxFit.contain,),),

      ),
      const SizedBox(height: 10.0),
      Center(
      child: Text(
      categoryList[index],
      style: const TextStyle(
      fontSize: 16,
      color: Color.fromRGBO(240, 86, 52, 1),
      ),
      ),
      ),
      ],
      ),
      ),
      ),
      );
      }, itemCount: categoryImages.length),
  ],),
      ),
    ),
    );

  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;


}
