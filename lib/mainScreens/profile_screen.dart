import 'package:alyjy/widgets/simple_app_bar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../authentication/auth_screen.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';
import '../modal/adress.dart';
import '../widgets/styled_text_widget.dart';
import 'adress_screen.dart';
import 'history_screen.dart';
import 'home_screen.dart';
import 'my_orders_screen.dart';
class ProfileScreen extends StatefulWidget {

const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with AutomaticKeepAliveClientMixin{
  makeDashboardItem(String title, IconData iconData, int index)
{
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child:
    Container(
      width:MediaQuery.of(context).size.width-30,
      height: 70,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: Colors.black54),),
      child: InkWell(
        onTap: ()
        {
          if(index == 0)
          {
            Navigator.push(context, MaterialPageRoute(builder: (c)=>MyOrdersScreen()));
          }
          if(index == 1)
          {
            Navigator.push(context, MaterialPageRoute(builder: (c)=> HistoryScreen()));
          }
          if(index == 2)
          {
            Navigator.push(context, MaterialPageRoute(builder: (c)=>AdressScreen()));
          }
          if(index == 3)
          {
            firebaseAuth.signOut().then((value){
              Navigator.push(context, MaterialPageRoute(builder: (c)=> const AuthScreen()));
            });
          }
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Icon(iconData, size: 35,color: Colors.black54),
                ),
                StyledTextWidget(text: title,size:20,weight:FontWeight.w500, color: Colors.black54),
          ],
        ),
            ),
            Icon(Icons.arrow_forward_ios, size: 35,color: Colors.black54,),
          ],
        ),
]),
    ),
  ));
}
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(child: WillPopScope(
      onWillPop: ()async =>false,
      child: Scaffold(
          appBar:AppBar(
            iconTheme: IconThemeData(
                color:Color.fromRGBO(72, 72, 72, 1), size: 25
            ),
            automaticallyImplyLeading: false,
            flexibleSpace: Container(
              color: Colors.white,
            ),
            centerTitle: true,
            title: StyledTextWidget(text: LocaleKeys.Profile.tr(),size: 24,spacing: 3,weight:FontWeight.bold,color: Color.fromRGBO(240, 86, 52, 1),),
          ),
          body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                CircleAvatar(
                radius: 80,
                backgroundImage: NetworkImage(
                    sharedPreferences!.getString("photoUrl")!
                ),),
                StyledTextWidget(text: sharedPreferences!.getString("name")!, size: 20,weight:FontWeight.bold)
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Container(
                    width:180,
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.cyan,
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.cyan),),
                    child: Center(child: StyledTextWidget(text: LocaleKeys.Update.tr(), color: Colors.white,)),
                  ),
                ),
                Container(
                  width:180,
                  height: 50,
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(240, 86, 52, 1),
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: Color.fromRGBO(240, 86, 52, 1)),),
                  child: Center(child: StyledTextWidget(text: LocaleKeys.Delete.tr(), color: Colors.white,)),
                ),
              ],
            ),
          ],
        ),
          const Divider(
            height: 10,
            color: Colors.grey,
            thickness: 2,
          ),
        SizedBox(height: 10,),
          makeDashboardItem(LocaleKeys.My_Orders.tr(), Icons.reorder, 0),
          makeDashboardItem(LocaleKeys.Orders_History.tr(), Icons.access_time_rounded, 1),
          makeDashboardItem(LocaleKeys.Add_New_Adress.tr(), Icons.add_location_rounded, 2),
          makeDashboardItem(LocaleKeys.Sign_Out.tr(), Icons.logout, 3),
        ],
      )),
    ));
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;


}
