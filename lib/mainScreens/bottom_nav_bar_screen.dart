import 'package:alyjy/mainScreens/cart_screen.dart';
import 'package:alyjy/mainScreens/home_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../generated/locale_keys.g.dart';
import 'category_screen.dart';
import 'profile_screen.dart';
class BottomNavBarScreen extends StatefulWidget {
  const BottomNavBarScreen({Key? key}) : super(key: key);

  @override
  State<BottomNavBarScreen> createState() => _BottomNavBarScreenState();
}

class _BottomNavBarScreenState extends State<BottomNavBarScreen> {
  int _bottomnavigationindex=0;
  final _screens=[
    HomeScreen(),
    CategoryScreen(),
    CartScreen(),
    ProfileScreen(),
  ];

  PageController? pageController;


  _onTapped(int index){
    setState(() {
      _bottomnavigationindex=index;
    });
    pageController!.jumpToPage(index);
  }

  void onPageChanged(int index){
    setState(() {
      _bottomnavigationindex=index;
    });
  }

  @override
  void initState() {
    super.initState();
    pageController=PageController();
  }

  @override
  void dispose() {
    pageController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
          currentIndex:_bottomnavigationindex,
          onTap: _onTapped,
          items: [
            BottomNavigationBarItem(
              icon:Icon(Icons.home,size:28,),
              label: LocaleKeys.Home.tr(),
            ),
            BottomNavigationBarItem(
              icon:Icon(Icons.apps_sharp,size: 28),
              label: LocaleKeys.Categories.tr(),
            ),
            BottomNavigationBarItem(
              icon:Icon(Icons.shopping_cart,size: 28),
              label: LocaleKeys.Cart.tr(),
            ),
            BottomNavigationBarItem(
                icon:Icon(Icons.person,size: 28),
                label: LocaleKeys.Profile.tr()
            ),
          ],
          type: BottomNavigationBarType.shifting,
          backgroundColor: Colors.white,
          selectedItemColor:  Color.fromRGBO(240, 86, 52, 1),
          unselectedItemColor: Colors.grey.shade400,
          showSelectedLabels: true,
          showUnselectedLabels: true,
        ),
      body:PageView(
        children: _screens,
        controller: pageController,
        onPageChanged: onPageChanged,
      )
    );
  }
}
