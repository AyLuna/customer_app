import 'package:alyjy/global/global.dart';
import 'package:alyjy/mainScreens/search_screen.dart';
import 'package:alyjy/widgets/styled_text_widget.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../generated/locale_keys.g.dart';
import '../modal/items.dart';
import '../widgets/home_screen_app_bar.dart';
import 'package:flutter/material.dart';
import '../widgets/language_picker.dart';
import '../widgets/progress_bar.dart';
import '../assistendMethods/assistant_methods.dart';
import '../widgets/header_text_widget.dart';
import '../modal/sellers.dart';
import '../widgets/sellers_ui_design.dart';
class HomeScreen extends StatefulWidget {
  final Items? model;

  const HomeScreen({Key? key, this.model}) : super(key: key);


  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with AutomaticKeepAliveClientMixin{
  final items=["slider/2.png", "slider/3.png", "slider/4.png", "slider/9.png", "slider/10.png", "slider/14.png", "slider/15.png", "slider/18.png", "slider/19.png", "slider/20.png"];


  @override
  void initState() {
    super.initState();

    clearCartNow(context);
  }


  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      child: Scaffold(
        appBar: HomeScreenAppBar(),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                height: MediaQuery.of(context).size.height*0.3,
                width: MediaQuery.of(context).size.width,
                child: CarouselSlider(items: items.map((e) {
                  return Builder(builder: (BuildContext context){
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Image.asset(e, fit: BoxFit.fill,),
                    );
                  });
                }).toList(),
                    options: CarouselOptions(
                      height: MediaQuery.of(context).size.height*0.3,
                      aspectRatio:1.91/1,
                      viewportFraction: 0.8,
                      initialPage: 0,
                      enableInfiniteScroll: true,
                      reverse: false,
                      autoPlay: true,
                      autoPlayInterval: Duration(seconds: 2),
                      autoPlayAnimationDuration: Duration(milliseconds: 500),
                      autoPlayCurve: Curves.decelerate,
                      enlargeCenterPage: true,
                      scrollDirection: Axis.horizontal,
                    )),
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(left: 10,top: 10),
                child: StyledTextWidget(text: LocaleKeys.Sellers.tr(), size: 24,spacing: 2,weight: FontWeight.w500),
              ),
            ),
            StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestore.instance.collection("sellers").snapshots(),
              builder: (context, snapshot){
                return !snapshot.hasData?SliverToBoxAdapter(
                  child: Center(child: circularProgress(),),
                ):
                SliverStaggeredGrid.countBuilder(
                  crossAxisCount: 2,
                  staggeredTileBuilder: (c) => StaggeredTile.fit(1),
                  itemBuilder: (context, index)
                  {
                    Sellers model = Sellers.fromJson(
                        snapshot.data!.docs[index].data()! as Map<String, dynamic>
                    );
                    //design for display sellers-cafes-restuarents
                    return Padding(
                      padding: const EdgeInsets.only(left:10, right: 10, top: 10),
                      child: SellersUiDesignWidget(
                        model: model,
                        context: context,
                      ),
                    );
                  },
                  itemCount: snapshot.data!.docs.length,
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

}
