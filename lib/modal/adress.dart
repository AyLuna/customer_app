class Adress{
  String? name;
  String? phoneNumber;
  String? flatNumber;
  String? city;
  String? state;
  String? fullAdress;
  double? lat;
  double? lng;

  Adress({
    this.name,
    this.phoneNumber,
    this.flatNumber,
    this.city,
    this.state,
    this.fullAdress,
    this.lat,
    this.lng,
  });

  Adress.fromJson(Map<String, dynamic>json){
    name = json["name"];
    phoneNumber = json["phoneNumber"];
    flatNumber = json["flatNumber"];
    city = json["city"];
    state = json["state"];
    fullAdress = json["fullAdress"];
    lat = json["lat"];
    lng = json["lng"];
  }

  Map<String, dynamic>toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>(); //Map<String, dynamic>["menuID"]
    data["name"] = this.name;
    data["phoneNumber"] = this.phoneNumber;
    data["flatNumber"] = this.flatNumber;
    data["city"] = this.city;
    data["state"] = this.state;
    data["fullAdress"] = this.fullAdress;
    data["lat"] = this.lat;
    data["lng"] = this.lng;
    return data;
  }
}
