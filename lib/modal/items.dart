import 'package:cloud_firestore/cloud_firestore.dart';

class Items
{
  String? sellerUID;
  String? itemID;
  String? title;
  String? categoryEng;
  Timestamp? publishedDate;
  String? thumbnailUrl;
  String? longDescription;
  String? status;
  int? price;

  Items({
    this.sellerUID,
    this.itemID,
    this.title,
    this.categoryEng,
    this.publishedDate,
    this.thumbnailUrl,
    this.longDescription,
    this.status,
  });

  Items.fromJson(Map<String, dynamic> json)
  {
    sellerUID = json['sellerUID'];
    itemID = json['itemID'];
    title = json['title'];
    categoryEng = json['categoryEng'];
    publishedDate = json['publishedDate'];
    thumbnailUrl = json['thumbnailUrl'];
    longDescription = json['longDescription'];
    status = json['status'];
    price = json['price'];
  }

  Map<String, dynamic> toJson()
  {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['sellerUID'] = sellerUID;
    data['itemID'] = itemID;
    data['title'] = title;
    data['categoryEng'] = categoryEng;
    data['price'] = price;
    data['publishedDate'] = publishedDate;
    data['thumbnailUrl'] = thumbnailUrl;
    data['longDescription'] = longDescription;
    data['status'] = status;

    return data;
  }
}