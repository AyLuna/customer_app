class Sellers{
  String? sellerName;
  String? sellerUID;
  String? sellerAvatarUrl;
  String? sellerEmail;
  String? sellerPhone;
  String? adress;

  Sellers({
    this.sellerName,
    this.sellerUID,
    this.sellerAvatarUrl,
    this.sellerEmail,
    this.sellerPhone,
    this.adress
  });

  Sellers.fromJson(Map<String, dynamic>json){
    sellerName = json["sellerName"];
    sellerUID = json["sellerUID"];
    sellerAvatarUrl = json["sellerAvatarUrl"];
    sellerEmail = json["sellerEmail"];
    sellerPhone = json["sellerPhone"];
    adress = json["adress"];
  }

    Map<String, dynamic> toJson(){
      final Map<String, dynamic> data=new Map<String, dynamic>();
      data["sellerName"]=this.sellerName;
      data["sellerUID"]=this.sellerUID;
      data["sellerAvatarUrl"]=this.sellerAvatarUrl;
      data["sellerEmail"]=this.sellerEmail;
      data["sellerPhone"]=this.sellerPhone;
      data["adress"]=this.adress;
      return data;
  }
}

