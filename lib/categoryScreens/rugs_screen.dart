import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../generated/locale_keys.g.dart';
import '../mainScreens/bottom_nav_bar_screen.dart';
import '../modal/items.dart';
import '../widgets/category_ui_design.dart';
import '../widgets/header_text_widget.dart';
import '../widgets/progress_bar.dart';
import '../widgets/simple_app_bar.dart';
import '../widgets/styled_text_widget.dart';
class RugsScreen extends StatefulWidget {
  final Items? model;
  const RugsScreen({Key? key, this.model}) : super(key: key);


  @override
  State<RugsScreen> createState() => _RugsScreenState();
}

class _RugsScreenState extends State<RugsScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: ()async =>false,
        child: Scaffold(
          appBar: SimpleAppBar(title:LocaleKeys.Rugs_Category.tr(), ),
          body:CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10,top: 10),
                  child: StyledTextWidget(text: LocaleKeys.All_Products.tr(), size: 24,spacing: 2,weight: FontWeight.w500),
                ),
              ),
              StreamBuilder<QuerySnapshot>(
                  stream: FirebaseFirestore.instance.collection("items").where("categoryEng", isEqualTo: "Rugs").orderBy("publishedDate", descending: true).snapshots(),
                  builder: (context, snapshot){
                    return !snapshot.hasData?SliverToBoxAdapter(
                      child: Center(child: circularProgress(),),
                    ):
                    SliverStaggeredGrid.countBuilder(crossAxisCount: 2, staggeredTileBuilder: (c)=>StaggeredTile.fit(1), itemBuilder: (context, index){
                      Items model=Items.fromJson(
                        snapshot.data!.docs[index].data()!as Map<String, dynamic>,
                      );
                      return Padding(
                        padding: const EdgeInsets.only(left:10, right: 10, top: 10),
                        child: CategoryUiDesignWidget(
                          model: model,
                          context: context,
                        ),
                      );
                    }, itemCount: snapshot.data!.docs.length);
                  }),
            ],
          ),
        ),
      ),
    );
  }
}