import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import '../assistendMethods/adress_changer.dart';
import '../assistendMethods/cart_item_coumter.dart';
import '../assistendMethods/total_amount.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../global/global.dart';
import 'package:provider/provider.dart';
import 'authentication/auth_screen.dart';
import 'mainScreens/bottom_nav_bar_screen.dart';

Future<void> main() async
{
  WidgetsFlutterBinding.ensureInitialized();

  sharedPreferences = await SharedPreferences.getInstance();

  await EasyLocalization.ensureInitialized();

  await Firebase.initializeApp();

  runApp(EasyLocalization(
      supportedLocales: [Locale('en'), Locale('tr'), Locale('ru')],
      path: 'assets/translations', // <-- change the path of the translation files
      fallbackLocale: Locale('en'),
      child: MyApp()
  ),);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (c)=>CartItemCounter()),
        ChangeNotifierProvider(create: (c)=>TotalAmount()),
        ChangeNotifierProvider(create: (c)=>AdressChanger()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Users App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        home: firebaseAuth.currentUser!=null?BottomNavBarScreen():AuthScreen(),
      ),

    );
  }
}
